package com.tboodle.papertrader.controller;

import com.tboodle.papertrader.domain.Trader;

import java.math.BigDecimal;
import java.util.UUID;

import com.tboodle.papertrader.exception.TraderNotFoundException;
import com.tboodle.papertrader.service.TraderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/trader")
public class TraderController {

  @Autowired
  private TraderService traderService;

  @GetMapping
  @ResponseStatus(HttpStatus.OK)
  public Iterable<Trader> getTraders() {
    return traderService.getAllTraders();
  }

  @GetMapping("/{traderName}")
  @ResponseStatus(HttpStatus.OK)
  public Trader getTrader(@PathVariable String traderName) {
    return traderService.getTrader(traderName).orElseThrow(() -> new TraderNotFoundException(traderName));
  }

  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  public Trader createTrader(@RequestParam(name = "traderName", required = false) String traderName,
                           @RequestParam(name = "liquidCash", required = false) Double liquidCash) {
   return traderService.registerTrader(new Trader(traderName, liquidCash));
  }

  @PutMapping("/buy")
  @ResponseStatus(HttpStatus.CREATED)
  public Trader buyPosition(@RequestParam(name = "traderName") String traderName,
                          @RequestParam(name = "positionName") String positionName,
                          @RequestParam(name = "numberOfShares") String numberOfShares) {
    Trader trader = traderService.getTrader(traderName).orElseThrow(() -> new TraderNotFoundException(traderName));
    return traderService.buyPosition(trader, positionName, Double.valueOf(numberOfShares));
  }

  @PutMapping("/sell")
  @ResponseStatus(HttpStatus.CREATED)
  public Trader sellPosition(@RequestParam(name = "traderName") String traderName,
                          @RequestParam(name = "positionName") String positionName,
                          @RequestParam(name = "numberOfShares") String numberOfShares) {
    Trader trader = traderService.getTrader(traderName).orElseThrow(() -> new TraderNotFoundException(traderName));
    return traderService.sellPosition(trader, positionName, Double.valueOf(numberOfShares));
  }
}
