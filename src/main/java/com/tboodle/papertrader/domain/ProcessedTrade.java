package com.tboodle.papertrader.domain;

import java.time.Instant;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Type;

@Data
@AllArgsConstructor
@Entity
public class ProcessedTrade {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Setter(AccessLevel.NONE)
  @Column(name = "id", unique = true, nullable = false, updatable = false)
  @Type(type = "org.hibernate.type.UUIDCharType")
  private UUID id;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "trader_id")
  @JsonBackReference
  Trader trader;

  @Column(name = "timeOfTrade", updatable = false, nullable = false)
  @Setter(AccessLevel.NONE)
  Instant timeOfTrade;

  @Column(name = "positionName", updatable = false, nullable = false)
  @Setter(AccessLevel.NONE)
  String positionName;

  @Column(name = "valueAtTrade", updatable = false, nullable = false)
  @Setter(AccessLevel.NONE)
  Double valueAtTrade;

  @Column(name = "TradeType", updatable = false, nullable = false)
  @Setter(AccessLevel.NONE)
  TradeType tradeType;

  public ProcessedTrade(Trader trader, Instant timeOfTrade, String positionName, Double valueAtTrade,
                        TradeType tradeType) {
    this.trader = trader;
    this.timeOfTrade = timeOfTrade;
    this.positionName = positionName;
    this.valueAtTrade = valueAtTrade;
    this.tradeType = tradeType;
  }
}
