package com.tboodle.papertrader.domain;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Type;

@Data
@AllArgsConstructor
@Entity
public class InvestmentPosition {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Setter(AccessLevel.NONE)
  @Column(name = "id", unique = true, nullable = false, updatable = false)
  @Type(type = "org.hibernate.type.UUIDCharType")
  private UUID id;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "trader_id")
  @JsonBackReference
  private Trader trader;

  @Column(name = "positionName", updatable = false, nullable = false)
  @Setter(AccessLevel.NONE)
  private String positionName;

  @Column(name = "shareCount", nullable = false)
  private Double shareCount;

  public InvestmentPosition(Trader trader, String positionName, Double shareCount) {
    this.trader = trader;
    this.positionName = positionName;
    this.shareCount = shareCount;
  }
}
