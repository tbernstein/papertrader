package com.tboodle.papertrader.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Type;

@Data
@AllArgsConstructor
@Entity
public class Trader {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Setter(AccessLevel.NONE)
  @Column(name = "id", unique = true, nullable = false, updatable = false)
  @Type(type = "org.hibernate.type.UUIDCharType")
  private UUID id;

  @Setter(AccessLevel.NONE)
  @Column(name = "name")
  private String name;

  @Column(name = "liquidCash")
  private Double liquidCash;

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "trader", cascade = CascadeType.ALL, orphanRemoval = true)
  private List<InvestmentPosition> investmentPositions;

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "trader", cascade = CascadeType.ALL, orphanRemoval = true)
  private List<ProcessedTrade> tradeHistory;

  public Trader(String name, Double liquidCash) {
    this.name = name;
    this.liquidCash = liquidCash;
    this.investmentPositions = new ArrayList<>();
    this.tradeHistory = new ArrayList<>();
  }
}
