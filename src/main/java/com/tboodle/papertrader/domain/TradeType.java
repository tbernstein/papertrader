package com.tboodle.papertrader.domain;

public enum TradeType {
  BUY, SELL
}
