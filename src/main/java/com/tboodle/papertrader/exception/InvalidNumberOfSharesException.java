package com.tboodle.papertrader.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class InvalidNumberOfSharesException extends RuntimeException {
  public InvalidNumberOfSharesException(Double shareCount, Double sharesToSell) {
    super("Tried to sell " + sharesToSell + " shares but this trader only has " + shareCount);
  }
}