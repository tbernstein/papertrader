package com.tboodle.papertrader.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class TraderNotFoundException extends RuntimeException {
  public TraderNotFoundException(String name) {
    super("Trader with the name " + name + " was not found");
  }
}