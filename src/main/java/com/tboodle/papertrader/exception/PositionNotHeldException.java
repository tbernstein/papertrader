package com.tboodle.papertrader.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class PositionNotHeldException extends RuntimeException {
  public PositionNotHeldException(String positionName) {
    super("Trader does not hold shares in " + positionName + " to be sold");
  }
}
