package com.tboodle.papertrader.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class UnknownSymbolException extends RuntimeException {
  public UnknownSymbolException(String positionName) {
    super("Unknown symbol: " + positionName);
  }
}
