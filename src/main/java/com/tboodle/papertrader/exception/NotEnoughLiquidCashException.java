package com.tboodle.papertrader.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class NotEnoughLiquidCashException extends RuntimeException {
  public NotEnoughLiquidCashException(Double currentLiquidCash, Double tradeCost) {
    super("Tried to buy shares worth " + tradeCost + " but trader only has " + currentLiquidCash + " available");
  }
}
