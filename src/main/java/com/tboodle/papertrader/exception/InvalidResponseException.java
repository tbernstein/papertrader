package com.tboodle.papertrader.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class InvalidResponseException extends RuntimeException {
  public InvalidResponseException(String positionName) {
    super("Invalid response for " + positionName + " request");
  }
}
