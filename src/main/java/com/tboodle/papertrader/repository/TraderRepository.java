package com.tboodle.papertrader.repository;

import java.util.Optional;
import java.util.UUID;

import com.tboodle.papertrader.domain.Trader;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.stereotype.Repository;

@Repository
public interface TraderRepository extends CrudRepository<Trader, UUID> {

  Optional<Trader> findByName(String name);
}
