package com.tboodle.papertrader.service;

import java.time.Instant;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tboodle.papertrader.domain.InvestmentPosition;
import com.tboodle.papertrader.domain.ProcessedTrade;
import com.tboodle.papertrader.domain.TradeType;
import com.tboodle.papertrader.domain.Trader;
import com.tboodle.papertrader.exception.InvalidNumberOfSharesException;
import com.tboodle.papertrader.exception.InvalidResponseException;
import com.tboodle.papertrader.exception.NotEnoughLiquidCashException;
import com.tboodle.papertrader.exception.PositionNotHeldException;
import com.tboodle.papertrader.exception.UnknownSymbolException;
import com.tboodle.papertrader.repository.TraderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;


@Service
public class TraderService {

  @Value("${secrets.iexKey:}")
  private String iexKey;

  @Autowired
  private TraderRepository traderRepository;

  public Trader registerTrader(Trader trader) {
    return traderRepository.save(trader);
  }

  public Optional<Trader> getTrader(String name) {
    return traderRepository.findByName(name);
  }

  public Iterable<Trader> getAllTraders() {
    return traderRepository.findAll();
  }

  public Trader buyPosition(Trader trader, String positionName, Double numberOfShares) {
    List<InvestmentPosition> investmentPositions = trader.getInvestmentPositions();
    Double currentPrice = getCurrentPrice(positionName);
    Double tradeCost = currentPrice * numberOfShares;
    InvestmentPosition positionToBuy =
            investmentPositions.stream().filter(position -> position.getPositionName().equals(positionName))
                               .findAny()
                               .orElse(null);

    if (tradeCost <= trader.getLiquidCash()) {
      if (positionToBuy == null) {
        investmentPositions.add(new InvestmentPosition(trader, positionName, numberOfShares));
      } else {
        investmentPositions.set(
                investmentPositions.indexOf(positionToBuy),
                new InvestmentPosition(trader, positionName,
                                       positionToBuy.getShareCount() + numberOfShares));
      }

      trader.setInvestmentPositions(investmentPositions);
      trader.setLiquidCash(trader.getLiquidCash() - tradeCost);
      List<ProcessedTrade> tradeHistory = trader.getTradeHistory();
      tradeHistory.add(new ProcessedTrade(trader, Instant.now(), positionName, currentPrice, TradeType.BUY));
      return traderRepository.save(trader);
    } else {
      throw new NotEnoughLiquidCashException(trader.getLiquidCash(), numberOfShares);
    }
  }

  public Trader sellPosition(Trader trader, String positionName, Double numberOfSharesToSell) {
    List<InvestmentPosition> investmentPositions = trader.getInvestmentPositions();
    InvestmentPosition positionToSell = investmentPositions.stream()
                                                           .filter(position -> position.getPositionName().equals(positionName))
                                                           .findAny()
                                                           .orElse(null);
    Double currentPrice = getCurrentPrice(positionName);

    if (positionToSell == null) {
      throw new PositionNotHeldException(positionName);
    }

    if (positionToSell.getShareCount() - numberOfSharesToSell < 0) {
      throw new InvalidNumberOfSharesException(positionToSell.getShareCount(), numberOfSharesToSell);
    } else {
      if (positionToSell.getShareCount().equals(numberOfSharesToSell)) {
        investmentPositions.remove(positionToSell);
      } else {
        investmentPositions.set(
                investmentPositions.indexOf(positionToSell),
                new InvestmentPosition(trader,
                                       positionName,
                                       positionToSell.getShareCount() - numberOfSharesToSell));
      }

      trader.setInvestmentPositions(investmentPositions);
      trader.setLiquidCash(trader.getLiquidCash() + (currentPrice * numberOfSharesToSell));
      List<ProcessedTrade> tradeHistory = trader.getTradeHistory();
      tradeHistory.add(new ProcessedTrade(trader, Instant.now(), positionName, currentPrice, TradeType.SELL));
      return traderRepository.save(trader);
    }
  }

  private Double getCurrentPrice(String positionName) {
    String requestUrl = "https://cloud.iexapis.com/stable/stock/" + positionName + "/quote?token=" + iexKey;
    RestTemplate restTemplate = new RestTemplate();
    ResponseEntity<String> response = restTemplate.getForEntity(requestUrl, String.class);

    if (response.getBody() == null) {
      throw new HttpServerErrorException(response.getStatusCode());
    }

    if (response.getBody().equals("Unknown Symbol")) {
      throw new UnknownSymbolException(positionName);
    }

    try {
      ObjectMapper objectMapper = new ObjectMapper();
      Map<String, String> responseAsObject = objectMapper.readValue(response.getBody(),
                                                                    new TypeReference<Map<String, String>>() {});
      return Double.parseDouble(responseAsObject.get("latestPrice"));

    } catch (Exception e) {
      throw new InvalidResponseException(positionName);
    }
  }
}
