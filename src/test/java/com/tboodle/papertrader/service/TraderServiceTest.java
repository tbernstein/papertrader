package com.tboodle.papertrader.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.tboodle.papertrader.domain.InvestmentPosition;
import com.tboodle.papertrader.domain.Trader;
import com.tboodle.papertrader.repository.TraderRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import static org.hamcrest.Matchers.anything;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;


@SpringBootTest
class TraderServiceTest {

  @Autowired
  TraderService traderService;

  @MockBean
  TraderRepository traderRepository;

  @Test
  @DisplayName("registerTrader successfully saves a trader")
  void traderServiceShouldReturnTraderWhenTheTraderIsRegistered() {
    Trader trader = new Trader("Tyler", 100.0);
    Mockito.when(traderRepository.save(trader)).thenReturn(trader);
    Trader returnedTrader = traderService.registerTrader(trader);
    assertThat(returnedTrader, equalTo(trader));
  }

  @Test
  @DisplayName("getTrader successfully returns a trader")
  void traderServiceShouldReturnTraderWhenTheTraderIsCalledFor() {
    Trader trader = new Trader("Tyler", 100.0);
    Mockito.when(traderRepository.findByName(trader.getName())).thenReturn(Optional.of(trader));
    Optional<Trader> returnedTrader = traderService.getTrader(trader.getName());
    if (!returnedTrader.isPresent())
      fail();
    assertThat(returnedTrader.get(), equalTo(trader));
  }

  @Test
  @DisplayName("getAllTraders successfully returns all traders")
  void traderServiceShouldReturnAllTradersWhenTheTradersAreCalledFor() {
    Trader trader1 = new Trader("Tyler", 100.0);
    Trader trader2 = new Trader("Jeremy", 200.0);
    List<Trader> traders = new ArrayList<>();

    traders.add(trader1);
    traders.add(trader2);

    Mockito.when(traderRepository.findAll()).thenReturn(traders);
    Iterable<Trader> returnedTraders = traderService.getAllTraders();
    assertThat(returnedTraders, equalTo(traders));
  }

  @Test
  @DisplayName("buyPosition for new position adds the transaction to the trader's positions and takes out the value "
          + "from the trader's cash")
  void traderServiceShouldAddPositionAndRemoveFromLiquidCashForNewPosition() {
    Trader trader = new Trader("Tyler", 10000.0);
    InvestmentPosition investmentPosition = new InvestmentPosition(trader, "AAPL", 2.0);
    List<InvestmentPosition> positions = new ArrayList<>();
    positions.add(investmentPosition);
    Trader traderWithPosition = new Trader("Tyler", 10000.0);
    traderWithPosition.setInvestmentPositions(positions);

    Mockito.when(traderRepository.save(Mockito.any())).thenReturn(traderWithPosition);
    Trader returnedTrader = traderService.buyPosition(trader, "AAPL", 2.0);
    assertThat(returnedTrader, equalTo(traderWithPosition));
  }

  @Test
  @DisplayName("sellPosition for all shares removes the position from the trader's positions and adds in the value "
          + "to the trader's cash")
  void traderServiceShouldRemovePositionAndAddToLiquidCash() {
    Trader trader = new Trader("Tyler", 100.0);
    InvestmentPosition investmentPosition = new InvestmentPosition(trader, "AAPL", 2.0);
    List<InvestmentPosition> positions = new ArrayList<>();
    positions.add(investmentPosition);
    Trader traderWithPosition = new Trader("Tyler", 100.0);
    traderWithPosition.setInvestmentPositions(positions);

    Mockito.when(traderRepository.save(Mockito.any())).thenReturn(trader);
    Trader returnedTrader = traderService.sellPosition(traderWithPosition, "AAPL", 2.0);
    assertThat(returnedTrader, equalTo(trader));
  }

}
