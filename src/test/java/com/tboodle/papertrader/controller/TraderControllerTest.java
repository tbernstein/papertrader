package com.tboodle.papertrader.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tboodle.papertrader.domain.Trader;
import com.tboodle.papertrader.service.TraderService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.core.Is.is;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class TraderControllerTest {

  private static ObjectMapper objectMapper = new ObjectMapper();

  @Autowired
  private MockMvc mockMvc;

  @MockBean
  private TraderService traderService;

  @Test
  @DisplayName("GET to /trader returns all registered traders and a 200")
  void getTraders() throws Exception {
    Trader trader1 = new Trader("Tyler", 100.0);
    Trader trader2 = new Trader("Jeremy", 200.0);
    List<Trader> traders = new ArrayList<>();

    traders.add(trader1);
    traders.add(trader2);

    Mockito.when(traderService.getAllTraders()).thenReturn(traders);

    this.mockMvc.perform(get("/trader")
                                 .with(user("user").password("password").roles("USER")))
                .andExpect(status().isOk())
                .andDo(print())
                .andExpect(content().string((objectMapper.writeValueAsString(traders))));
  }

  @Test
  @DisplayName("GET to /trader/Tyler returns the trader named Tyler and a 200")
  void getSpecificTrader() throws Exception {
    Trader trader = new Trader("Tyler", 100.0);

    Mockito.when(traderService.getTrader("Tyler")).thenReturn(Optional.of(trader));

    this.mockMvc.perform(get("/trader/Tyler")
                                 .with(user("user").password("password").roles("USER")))
                .andExpect(status().isOk())
                .andDo(print())
                .andExpect(content().string((objectMapper.writeValueAsString(trader))));
  }

  @Test
  @DisplayName("POST to /trader returns a 201 for registering a trader and the trader")
  void registerNewTrader() throws Exception {
    Trader trader = new Trader("Tyler", 100.0);

    Mockito.when(traderService.registerTrader(trader)).thenReturn(trader);

    this.mockMvc.perform(post("/trader")
                                 .with(user("user").password("password").roles("USER"))
                                 .param("traderName", "Tyler")
                                 .param("liquidCash", "100"))
                .andExpect(status().isCreated())
                .andExpect(content().string((objectMapper.writeValueAsString(trader))))
                .andDo(print());
  }

  @Test
  @DisplayName("PUT to /trader/buy returns a 201 for buying a new position and the trader")
  void buyNewPosition() throws Exception {
    Trader trader = new Trader("Tyler", 100.0);

    Mockito.when(traderService.getTrader("Tyler")).thenReturn(Optional.of(trader));
    Mockito.when(traderService.buyPosition(trader, "AAPL", 10.0)).thenReturn(trader);

    this.mockMvc.perform(put("/trader/buy")
                                 .with(user("user").password("password").roles("USER"))
                                 .param("traderName", "Tyler")
                                 .param("positionName", "AAPL")
                                 .param("numberOfShares", "10"))
                .andExpect(status().isCreated())
                .andExpect(content().string((objectMapper.writeValueAsString(trader))))
                .andDo(print());
  }

  @Test
  @DisplayName("PUT to /trader/sell returns a 201 for selling a position and the trader")
  void sellPosition() throws Exception {
    Trader trader = new Trader("Tyler", 100.0);

    Mockito.when(traderService.getTrader("Tyler")).thenReturn(Optional.of(trader));
    Mockito.when(traderService.sellPosition(trader, "AAPL", 10.0)).thenReturn(trader);

    this.mockMvc.perform(put("/trader/sell")
                                 .with(user("user").password("password").roles("USER"))
                                 .param("traderName", "Tyler")
                                 .param("positionName", "AAPL")
                                 .param("numberOfShares", "10"))
                .andExpect(status().isCreated())
                .andExpect(content().string((objectMapper.writeValueAsString(trader))))
                .andDo(print());
  }
}
