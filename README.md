# PaperTrader

PaperTrader is a Spring Boot application that provides users with the ability to test trading strategies by creating trades using fake money.